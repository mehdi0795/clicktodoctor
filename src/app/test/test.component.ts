import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import {Router} from '@angular/router'
import { AppComponent } from '../app.component';
import { MedecinService } from '../services/medecin.service';
import { Data } from '@syncfusion/ej2-schedule/src/schedule/actions/data';
import { PremiereInscription } from '../medecin/medecin.component';
import { UserService } from '../services/user.service';
import { LoginComponent } from '../login/login.component';
import { Indisponibilite } from '../calendrier2/calendrier2.component';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;

  nom=new FormControl('', [Validators.required]);
  prenom:String;
  telephone:String;
  email= new FormControl('', [Validators.required, Validators.email]);
  nrpps:String;
  nadeli:String;
  specialite:string[];
  
  motdepasse:String;
  confirmermotdepasse:String;
  hide = true;
  newUser=new Medecin();
  user=new User();
  ok=false;
  isMedecin = false;
  erreurConfirmationMdp=false;
  mdpErreur=true;
  role=new Role();

  specialites = new FormControl();

  // SpecialiteList: string[] = ['Dermatologie', 'Hématologie', 'Neurologie', 'Urologie', 'Rhumatologie', 'Ophtalmologie'];
  SpecialiteList: Specialite[] ;

  premiereInscription=new PremiereInscription();
  constructor(
    private _router: Router,
    
    private medecinService: MedecinService,
    private formBuilder: FormBuilder,
    private userService: UserService


  ) { }

  ngOnInit() {
    // this.role.id=1;
    this.role.name='medecin';
    this.medecinService.getAllSpecialites().subscribe(response=>{
      this.SpecialiteList=response;
      // console.log(this.SpecialiteList);
    });

    this.registerForm = this.formBuilder.group({
      Nom: ['', Validators.required],
      Prenom: ['', Validators.required],
      Telephone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      nrpps: ['', Validators.required],
      adeli: ['', Validators.required],
      motdepasse: ['', [Validators.required, Validators.minLength(8)]],
      confirmermotdepasse: ['', Validators.required]
  });
    

  }
  get f() { return this.registerForm.controls; }

  EnregistrerMedecin(){
    
    this.newUser.nom=this.registerForm.value.Nom;
    this.newUser.prenom=this.registerForm.value.Prenom;
    this.newUser.tel=this.registerForm.value.Telephone;
   // this.newUser.telephone=this.telephone;
    //this.newUser.email=this.email;
    this.newUser.rpps=this.registerForm.value.nrpps;
    this.newUser.adeli=this.registerForm.value.adeli;
    
    //this.newUser.password=this.motdepasse;
    //this.newUser.matchingPassword=this.confirmermotdepasse;
    this.premiereInscription.specs=this.specialite;

    this.user.email=this.registerForm.value.email;
    this.user.password=this.registerForm.value.motdepasse;

    this.premiereInscription.medecin=this.newUser;
    this.premiereInscription.user=this.user;

    // sessionStorage.setItem('User',JSON.stringify(this.premiereInscription));
    // console.log('ok');
    // console.log(this.nom);
    // console.log(this.prenom);
    // console.log(this.telephone);
    // console.log(this.email);
    // console.log(this.nrpps);
    // console.log(this.nadeli);
    // console.log(this.specialite);
    // console.log(this.motdepasse);
    // this.isMedecin=true;
    // console.log(this.user);
    console.log(this.premiereInscription);
    //sessionStorage.setItem('isMedecin',JSON.stringify(this.isMedecin));
    //this.appComponent.isMedecinConnected();
    

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }
     this.medecinService.inscrireMedecin(this.premiereInscription).subscribe(response=>{
      console.log('bravo');
      console.log(response);
      this.userService.connexion(this.premiereInscription.user.email).subscribe(response=>{
        sessionStorage.setItem('User',JSON.stringify(response));
      });
      this.userService.isMedecin=true;
      this.userService.MessageBienvenue=true;
      this._router.navigate(['/calendrier2']);

    
    },error=>console.log('erreur')
    );

    //  this.medecinService.getAllTest().subscribe(data=>{
    //    console.log(data);
    //   console.log('i ma hereeee '); 
    //   });
    
    
   
    //this._router.navigateByUrl('/test', {skipLocationChange: true}).then(()=>
   // this._router.navigate(['/espaceMedecin']); 
    
    

  }

  close(){
    this.erreurConfirmationMdp=false;
  }

  verifierMotDePasse(mdp1,mdp2){
    if (mdp1==mdp2){
      this.submitted = true;
      this.erreurConfirmationMdp=false;
      this.mdpErreur=false;

    }
    else{
      this.submitted = false;
      this.erreurConfirmationMdp=true;
      this.mdpErreur=true;
    }
  }

  getNomError(){
    return this.nom.hasError('required') ? 'You must enter a name' : '';
  }
  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }
}


export class FormFieldErrorExample {
  email = new FormControl('', [Validators.required, Validators.email]);
  nom = new FormControl('', [Validators.required]);

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }
  getNomError(){
    return this.nom.hasError('required') ? 'You must enter a value' : '';
  }
}

export class NouveauMedecin{
  public nom:String;
  public prenom:String;
 
  public tel:String;
  //public email:String;
  public rpps:String;
  public adeli:String;
  //public password:String;
  //public matchingPassword:String;
 // public specs:string[];
  

  
  constructor(){}
}

export class Medecin{
  public nom:String;
  public prenom:String;
  public tel:String;
  public rpps:String;
  public adeli:String;
  public specialites:Specialite[];
  public sexe:string;
  public dateDeNaissance:string;
  public adresse:string;
  public region:string;
  public departement:string;
  public codePostal:string;
  public datePriseDiplome:string;
  public universite:string;
  public indisponibilites:Indisponibilite[];
  
  
  constructor(){}
}


export class User{
  public matricule:number;
  public email:String;
 
  public password:String;
  public medecin:Medecin;
  //public matchingPassword:String;
 // public specs:string[];
  

  
  constructor(){}
}

export class Test {
  nom:String;
  prenom:String

  constructor(){}
}

export class Role {
  id:number;
  name:String;
  

  constructor(){}
}
export class Specialite {
  nomSpecialite:string;
  
  

  constructor(){}
}