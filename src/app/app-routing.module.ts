import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';
import { PatientComponent } from './patient/patient.component';
import { MedecinComponent } from './medecin/medecin.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { TestComponent } from './test/test.component';
import { EspaceMedecinComponent } from './espace-medecin/espace-medecin.component';
import { ModifierCompteComponent } from './modifier-compte/modifier-compte.component';
import { CalendrierMedecinComponent } from './calendrier-medecin/calendrier-medecin.component';
import { DetailsPatientComponent } from './details-patient/details-patient.component';
import { AppelVideoComponent } from './appel-video/appel-video.component';
import { Calendrier2Component } from './calendrier2/calendrier2.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'admin',
    component: AdminComponent
  },
  {
    path: 'patient',
    component: PatientComponent
  },
  {
    path: 'medecin',
    component: MedecinComponent
  },
  {
    path: 'auth/login',
    component: LoginComponent
  },
  {
    path: 'signup',
    component: RegisterComponent
  },
  {
    path: 'test',
    component: TestComponent
  },
  {
    path: 'espaceMedecin',
    component: EspaceMedecinComponent
  },
  {
    path: 'modifierCompte',
    component: ModifierCompteComponent
  },
  {
    path: 'calendrierMedecin',
    component: CalendrierMedecinComponent
  },
  {
    path: 'calendrier2',
    component: Calendrier2Component
  },
  {
    path: 'detailsPatient/:id',
    component: DetailsPatientComponent
  },
  {
    path: 'appelVideo/:id',
    component: AppelVideoComponent
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
