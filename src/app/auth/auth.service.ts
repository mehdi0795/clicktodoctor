import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtResponse } from './jwt-response';

import { LoginInfo } from './LoginInfo';
import { SignupInfo } from './SignupInfo';
import { map, catchError } from 'rxjs/operators';


const httpOptions = {
  headers: new HttpHeaders({'content-type':'application/json'})
};
/*
const httpsOptions = {
  headers: new HttpHeaders({'content-type':'application/json'}),
  response: 'text'
};*/

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loginUrl = 'http://localhost:8088/clicktodoctor/signin';
  private signupUrl = 'http://localhost:8088/clicktodoctor/inscription/medecin';

  constructor(private http: HttpClient){ }

  login(credentials: LoginInfo): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(this.loginUrl,credentials,httpOptions);
  }

  signup(signUpInfo: SignupInfo): Observable<any>{
    return this.http.post<any>(this.signupUrl,signUpInfo,httpOptions);

  }
}
