export class SignupInfo {
    nom: string;
    prenom: string;
    username: string;
    email: string;
    password: string;
    matchingPassword: string;
    //roles: string[];
    specs: string[];

    constructor(nom: string, prenom: string, username: string, email: string, password: string, matchingPassword: string, specs: string[]){
        this.nom = nom;
        this.prenom = prenom;
        this.username = username;
        this.email = email;
        this.password = password;
        this.matchingPassword = matchingPassword;
        this.specs = specs;
    }

    public setSpecs(specs : string[]) : void {
        this.specs = specs;
    }


}