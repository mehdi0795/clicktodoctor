import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from './auth/token-storage.service';
import { Router } from '@angular/router';
import { NouveauMedecin } from './test/test.component';
import { UserService } from './services/user.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  private roles: string[];
  private authority: string;
  private loggedin = false;
  medecin=new NouveauMedecin();
  isMedecin:boolean;

  constructor(private tokenStorage : TokenStorageService, private _router: Router,private userService:UserService){}
  

  ngOnInit() {
    // this.isMedecin=JSON.parse(sessionStorage.getItem('isMedecin'));
    this.medecin=JSON.parse(sessionStorage.getItem('User.medecin'));
    if (this.isMedecin!=null) this.userService.isMedecin=true;
    this.isMedecin=this.userService.isMedecin;
    console.log('am in app component');
    console.log(this.isMedecin);
    
    console.log(this.isMedecin);
    console.log('hello');
    console.log(this.medecin);

    
    if(this.tokenStorage.getToken()){
      this.loggedin = true;
      console.log("loggedin : "+this.loggedin);
      this.roles = this.tokenStorage.getAuthorities();
      this.roles.every(role => {
        if( role === 'ROLE_ADMIN') {
          this.authority = 'admin';
          return false;
        }else if(role === 'ROLE_MEDECIN'){
          this.authority = 'medecin';
          return false;
        }
        this.authority = 'patient';
        return true;
      });
    }
    //console.log(" authority :"+this.authority)
  }

  isMedecinConnected(){
    this.medecin=JSON.parse(sessionStorage.getItem('Medecin'));
    this.isMedecin=true;
    console.log("je suis dans app compo")
    this._router.navigate(['/espaceMedecin']);
  }



  logOut(){
    localStorage.clear();
    this.loggedin = false;
    this.userService.isMedecin=false;
    this.tokenStorage.signOut();
    //window.location.reload();
    this._router.navigate(['/home']);
  }

  PagemodifierCompte(){
    this._router.navigate(['/modifierCompte']);
  }

  pageInscription(){
    this._router.navigate(['/test']);
  }
 
}
