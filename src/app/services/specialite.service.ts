import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpecialiteService {

  private specialitesURL = 'http://localhost:8088/clicktodoctor/specialites';
                                   

  constructor(private http : HttpClient) { }

  getSpecialites(){

    return this.http.get(this.specialitesURL, { responseType : 'text' });
  }
}
