import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Indisponibilite } from '../calendrier2/calendrier2.component';

@Injectable({
  providedIn: 'root'
})
export class CalendrierService {

  constructor(private http : HttpClient) { }



  ajouterIndisponibilite(indisponibilite){
    
    return this.http.post(`http://localhost:8088/clicktodoctor/ajouter/indisponibilite`,indisponibilite, {responseType: 'text'});
  }

  tousLesIndisponibilites(){
    return this.http.get<Indisponibilite[]>(`http://localhost:8088/clicktodoctor/allIndisponibiltes`);
  }
  
  supprimerIndisponibilite(id){
    return this.http.delete(`http://localhost:8088/clicktodoctor/supprimerIndisponibilite/${id}`);
  }

  modifierIndisponibilite(indisponibilite){
    return this.http.put(`http://localhost:8088/clicktodoctor/modifier/indisponibilite`,indisponibilite);
  }
  

  getIndisponibiliteById(id){
    return this.http.get<Indisponibilite>(`http://localhost:8088/clicktodoctor/getIndispoById/${id}`);
  }

  getIndisponibiliteByMedecin(medecin){
    return this.http.get<Indisponibilite[]>(`http://localhost:8088/clicktodoctor/getIndispoByMedecin`,medecin);
  }

}
