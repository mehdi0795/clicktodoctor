import { Injectable, Optional } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../test/test.component';
import { Route, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  isMedecin=false;
  miseAJour=false;
  private adminsUrl = 'http://localhost:8088/clicktodoctor/admin';
  private patientsUrl = 'http://localhost:8088/clicktodoctor/patients';
  private medecinsUrl = 'http://localhost:8088/clicktodoctor/medecins';
  MessageBienvenue=false;
  constructor(private http : HttpClient,private router:Router) { }

  getEspaceAdmin(): Observable<string>{
    return this.http.get(this.adminsUrl, { responseType : 'text' });
  }

  getEspaceMedecin(){
    return this.http.get(this.medecinsUrl, { responseType : 'text' });
  }

  getEspacePatient(){
    return this.http.get(this.patientsUrl, { responseType : 'text' });
  }

  modifierMedecin(id,modifierInscription){
    return this.http.put(`http://localhost:8088/clicktodoctor/modifierMedecin/${id}`,modifierInscription);
  }

  connexion(mail){
    console.log(mail);
    console.log('am in user service');
    return this.http.get<User>(`http://localhost:8088/clicktodoctor/getUserByEmail/${mail}`);
  }

  connexionMedecin()
  {
    this.isMedecin=true;
    this.miseAJour=false;
    this.router.navigate(['/espaceMedecin']);

  }

  getUserByEmail(email){
    return this.http.get<User>(`http://localhost:8088/clicktodoctor/getUserByEmail/${email}`);
  }
  
}
