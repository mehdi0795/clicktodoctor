import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NouveauMedecin, Test, Specialite } from '../test/test.component';

@Injectable({
  providedIn: 'root'
})
export class MedecinService {

  constructor(private http : HttpClient) { }

  inscrireMedecin(premiereInscription){
    console.log('am medecin service')
    console.log(premiereInscription.user);
    console.log(premiereInscription.medecin);
    
    return this.http.post(`http://localhost:8088/clicktodoctor/inscription/premiereInscription`,premiereInscription, {responseType: 'text'});
  }
//, {responseType: 'text'}
  getAllTest(){
  return this.http.get<Test[]>(`http://localhost:8088/clicktodoctor/rec`);
}

getAllSpecialites(){
  return this.http.get<Specialite[]>(`http://localhost:8088/clicktodoctor/getAllSpecilites`);
}


}
