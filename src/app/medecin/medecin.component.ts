import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { User, NouveauMedecin, Specialite, Medecin } from '../test/test.component';

@Component({
  selector: 'app-medecin',
  templateUrl: './medecin.component.html',
  styleUrls: ['./medecin.component.css']
})
export class MedecinComponent implements OnInit {

  board : string;
  errorMessage : string;

  constructor(private userService : UserService) { }

  ngOnInit() {

    this.userService.getEspaceMedecin().subscribe(
      data => {
        console.log("Accès Medecin");
        this.board = data;
      },
      error =>{
        this.errorMessage = `${error.status} : ${JSON.parse(error.error).message}`;
      }
    );
  }

}

export class PremiereInscription{
  user:User;
  medecin:Medecin;
  specs:String[];
   constructor(){}
}
