import { Component, OnInit } from '@angular/core';
import { SignupInfo } from '../auth/SignupInfo';
import { AuthService } from '../auth/auth.service';
import { SpecialiteService } from '../services/specialite.service';

import { error } from 'util';


const roles : any = ['ROLE_ADMIN','ROLE_MEDECIN','ROLE_PATIENT'];

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form : any = {};
  signupInfo : SignupInfo;
  isSignedUp = false;
  isSignUpFailed = false;
  errorMessage = '';
  allSpecialites: any; 
  //allSpecialites = ['Gynéco_Grossesse','Pédiatrie','Médecine_générale','Psychologie','UROLOGUE','Dermatologie',
	//'Homéopathie','Médecine_du_sommeil','Neurologie'];
  
  constructor(private authService : AuthService, private specialiteService : SpecialiteService) { }

  ngOnInit() {

    
    this.specialiteService.getSpecialites().subscribe((res) =>{  
      this.allSpecialites = JSON.parse(res);
      let ls: string[] = this.allSpecialites;
      for (let i=0; i < ls.length; i++) {
      console.log("specialite n° "+(i+1)+" = " + ls[i]);
    }
    });
    


  }

  onSubmit(){
    console.log(this.form);

    this.signupInfo = new SignupInfo(
      this.form.nom,
      this.form.prenom,
      this.form.username,
      this.form.email,
      this.form.password,
      this.form.matchingPassword,
      this.form.specs
    );

    //console.log(" SPECS = "+JSON.parse(this.form.specs));
    //this.signupInfo.setSpecs(this.allSpecialites);

    console.log(this.signupInfo.specs);

    this.authService.signup(this.signupInfo).subscribe(
      (res) => {
        console.log(JSON.stringify(res));
      },
      data => {
        this.isSignedUp = true;
        console.log('IsSignedUp ? => '+this.isSignedUp);
        this.isSignUpFailed = false;
        console.log('IsSignedUpFailed ? => '+this.isSignUpFailed);
      }
      /*,
      error => {
        console.log(error.error);
        this.errorMessage = error.error.message;
        this.isSignUpFailed = true;
      }*/
    );

    console.log(this.form);
    let medecinSpecs: string[] = this.signupInfo.specs;
    for (let i=0; i < medecinSpecs.length; i++) {
    console.log("specialite n° "+i+" = " + medecinSpecs[i]);
    }

  }

}
