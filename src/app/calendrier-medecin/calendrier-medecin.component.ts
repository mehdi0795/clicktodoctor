import { Component, OnInit } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
@Component({
  selector: 'app-calendrier-medecin',
  templateUrl: './calendrier-medecin.component.html',
  styleUrls: ['./calendrier-medecin.component.css']
})
export class CalendrierMedecinComponent implements OnInit {

  
  calendarPlugins=[dayGridPlugin];
  calendarEvents:any[]=[{start:'2019-07-07T08:00:00.000Z', title:'indisponible', end: "2019-07-07T11:00:00.000Z",color: {
    primary: "#ff0000",
    secondary: "#FAE3E3"
    
  }
 
}];
  constructor() { }

  ngOnInit() {
  }

}
