import { Component, OnInit, ViewChild } from '@angular/core';
import { NouveauMedecin, User } from '../test/test.component';
import { MatTableDataSource, MatPaginator, MatTreeFlattener, MatTreeFlatDataSource, MatSort, Sort } from '@angular/material';
import { FlatTreeControl } from '@angular/cdk/tree';
import { AppComponent } from '../app.component';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

export interface RendezVous{
  id:number;
  nom:string;
 prenom:string;
  telephone:string;
 date:string;
  etat:string;
 cv:string;
 jourj:boolean;
 

 
}
const listRV: RendezVous[]=[{id:8,nom:"chelly",prenom:"mehdi",telephone:"26839421",date:"2019/07/09 10h",etat:"Non payé",cv:"12345678",jourj:true},
  {id:1,nom:"test",prenom:"test",telephone:"22839421",date:"2019/07/12 15h",etat:"Confirmé",cv:"12345678",jourj:false},
  {id:2,nom:"nom",prenom:"prenom",telephone:"26839421",date:"2019/11/07 16h",etat:"Confirmé",cv:"12345678",jourj:false},
  {id:3,nom:"aaa",prenom:"aa",telephone:"26839421",date:"2019/12/08 10h",etat:"Confirmé",cv:"12345678",jourj:false},
  {id:4,nom:"bb",prenom:"bbb",telephone:"26839421",date:"2019/07/10 12h",etat:"Non payé",cv:"12345678",jourj:true},
  {id:5,nom:"ccc",prenom:"ccc",telephone:"26839421",date:"2019/12/25 15h",etat:"Annulé",cv:"12345678",jourj:true},
  {id:6,nom:"dddd",prenom:"dddd",telephone:"26839421",date:"2019/12/01 08h",etat:"Non payé",cv:"12345678",jourj:true},
  {id:7,nom:"eeee",prenom:"eeee",telephone:"26839421",date:"2019/08/01 17h",etat:"Non payé",cv:"12345678",jourj:true}
];


@Component({
  selector: 'app-espace-medecin',
  templateUrl: './espace-medecin.component.html',
  styleUrls: ['./espace-medecin.component.css']
})
export class EspaceMedecinComponent implements OnInit {
 

  
   

  medecin=new NouveauMedecin();
  displayedColumns: string[] = ['Nom et Prénom du patient','Téléphone','carte vitale','Date et heure du rendez-vous','Etat','Appel video','dossier medical','Analyse','Facture','Ordonnance'];
  dataSource = new MatTableDataSource();
  mehdi='mehdi';
  nomMedecin:String;
  prenomMedecin:String;
  user=new User();
  


  constructor(private appComponent: AppComponent, private router:Router,private userService:UserService) {}
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  

  ngOnInit(
    
  ) {
    
    // sessionStorage.setItem('isMedecin',JSON.stringify(true));
    // this.appComponent.isMedecin=true;
    // this.medecin=JSON.parse(sessionStorage.getItem('User'));
    // this.nomMedecin=this.medecin.nom;
    // this.prenomMedecin=this.medecin.prenom;

    // console.log(this.medecin);
    // this.nomMedecin=this.medecin.nom;
    // this.prenomMedecin=this.medecin.prenom;
    this.user=JSON.parse(sessionStorage.getItem('User'));
    this.userService.isMedecin=true;
    this.nomMedecin=this.user.medecin.nom;
    this.prenomMedecin=this.user.medecin.prenom;

    this.dataSource=new MatTableDataSource(listRV);
    this.dataSource.paginator=this.paginator;
    this.dataSource.sort = this.sort;

  }

  sortData(sort: Sort) {
    const data = listRV.slice();
    if (!sort.active || sort.direction === '') {
      this.dataSource.data = data;
      return;
    }

    this.dataSource.data = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'date': return compare(a.date, b.date, isAsc);
        case 'telephone': return compare(a.telephone, b.telephone, isAsc);
      
        default: return 0;
      }
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  mm(){
    console.log('homeeeeeeeee');
  }
  close(){
    this.userService.miseAJour=false;
  }

  pageDetail(id){
    console.log(id);
    this.router.navigate(['/detailsPatient/',id]);

  }
  pageAppelVideo(id){
    this.router.navigate(['/appelVideo/',id]);
  }

}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}


