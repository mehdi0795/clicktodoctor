import { Component, OnInit } from '@angular/core';
import { EventSettingsModel } from '@syncfusion/ej2-schedule';
import { extend } from '@syncfusion/ej2-base';
import { DayService, WeekService, WorkWeekService, MonthService, AgendaService, ResizeService, DragAndDropService } from '@syncfusion/ej2-angular-schedule';
import { UserService } from '../services/user.service';
import { EventSettings } from '@syncfusion/ej2-schedule/src/schedule/models/event-settings';
import { SELECT_PANEL_INDENT_PADDING_X, throwToolbarMixedModesError } from '@angular/material';
import { User, Medecin } from '../test/test.component';
import { CalendrierService } from '../services/calendrier.service';
//import {scheduleData} from './data';

@Component({
  selector: 'app-calendrier2',
  templateUrl: './calendrier2.component.html',
  styleUrls: ['./calendrier2.component.css'],
  providers: [DayService, WeekService, WorkWeekService, MonthService, AgendaService, ResizeService, DragAndDropService]
})
export class Calendrier2Component implements OnInit {
  informations='helloChelly';
  listeIndisponible:Indisponibilite[];
  // scheduleData3:Object[]=[];
  public selectedDate: Date = new Date();
  public eventSettings: any;
  erreurManipulation=false;
  renitialisation=false;
  user=new User();
  indisponibilite=new Indisponibilite();
  eventupdated=false;
  eventdeleted=false;
  eventadded=false;
  nbeventupdated=0;
  nbeventdeleted=0;
  nbeventadded=0;

  constructor(private userService:UserService,private calendrierService:CalendrierService) { }

  ngOnInit() {
    
    this.userService.isMedecin=true;
    // this.user=JSON.parse(sessionStorage.getItem('User'));
   
    // console.log('la liste des indisponibilites');
    // console.log(this.user.medecin.indisponibilites);
    this.chargerLesEvenements()

    console.log(scheduleData3);
    console.log(this.eventSettings);

    this.renitialisation=false;
    
  }


  chargerLesEvenements(){
    this.user=JSON.parse(sessionStorage.getItem('User'));
    scheduleData3=[];
    listeIndisponible=this.user.medecin.indisponibilites;
    for(let i=0;i<listeRDV.length;i++){
      
      scheduleData3[i]=listeRDV[i];
    } 
    // this.calendrierService.tousLesIndisponibilites().subscribe(response=>{
     
    //   listeIndisponible=response;
    //   for(let i=0;i<listeIndisponible.length;i++){
    //     scheduleData3[listeRDV.length+i]=new Object();
    //     scheduleData3[listeRDV.length+i]['Id']=listeIndisponible[i]['id_indisponibilite'];
    //     scheduleData3[listeRDV.length+i]['Subject']=listeIndisponible[i]['description'];
    //     scheduleData3[listeRDV.length+i]['StartTime']=listeIndisponible[i]['dateDebutIndisponibilite'];
    //     scheduleData3[listeRDV.length+i]['EndTime']=listeIndisponible[i]['dateFinIndisponibilite'];
        
       
    //   } 
    //   this.eventSettings={ dataSource: <Object[]>extend([], scheduleData3, null, true)};
    //   console.log(scheduleData3);

    // });
    for(let i=0;i<listeIndisponible.length;i++){
          scheduleData3[listeRDV.length+i]=new Object();
          scheduleData3[listeRDV.length+i]['Id']=listeIndisponible[i]['id_indisponibilite'];
          scheduleData3[listeRDV.length+i]['Subject']=listeIndisponible[i]['description'];
          scheduleData3[listeRDV.length+i]['StartTime']=listeIndisponible[i]['dateDebutIndisponibilite'];
          scheduleData3[listeRDV.length+i]['EndTime']=listeIndisponible[i]['dateFinIndisponibilite'];
          
         
        } 
        this.eventSettings={ dataSource: <Object[]>extend([], scheduleData3, null, true)};
  }


  afficher(){
    // verification de la suppression d une consultation
    let ok=true;
    for(let i=0;i<listeRDV.length;i++){
      console.log(listeRDV[i]);
      console.log(this.eventSettings.dataSource[i]);
      // ||(scheduleData3[i][1]!=this.eventSettings.dataSource[i][1])||(scheduleData3[i][2]!=this.eventSettings.dataSource[i][2])||(scheduleData3[i][3]!=this.eventSettings.dataSource[i][3])
      if ((listeRDV[i]['Id']!=this.eventSettings.dataSource[i].Id)||(listeRDV[i]['StartTime']!=this.eventSettings.dataSource[i].StartTime)||(listeRDV[i]['EndTime']!=this.eventSettings.dataSource[i].EndTime)||(listeRDV[i]['Subject']!=this.eventSettings.dataSource[i].Subject))
      {
        console.log(listeRDV[i]);
        console.log(this.eventSettings.dataSource[i]);
        this.erreurManipulation=true;
        ok=false;
        console.log('vous avez supprimé un rdv!! le calendrier sera rénitialisé');
        break;
        
      }
      console.log(scheduleData3[i]['Id']);
      console.log(this.eventSettings.dataSource[i].Id);
    
    }
    //fin verification
    //debut parcours de la nouvelle liste 
    // il n y a pas eu de suppression d une consultation ok =  true
    if (ok==true){
      
      console.log('bravo modifications sauvegardées ');
      // console.log(this.eventSettings.dataSource.length);
      // console.log(this.eventSettings.dataSource);

      // if (this.eventSettings.dataSource.length>listeRDV.length+listeIndisponible.length)
      //   {
      //     for(let i=listeRDV.length+listeIndisponible.length;i<this.eventSettings.dataSource.length;i++){
      //       this.indisponibilite.description=this.eventSettings.dataSource[i].Subject;
      //       this.indisponibilite.dateDebutIndisponibilite=this.eventSettings.dataSource[i].StartTime;
      //       this.indisponibilite.dateFinIndisponibilite=this.eventSettings.dataSource[i].EndTime;
      //       this.indisponibilite.medecin=this.user.medecin;
      //       this.calendrierService.ajouterIndisponibilite(this.indisponibilite).subscribe(response=>{
      //         console.log('indisponibilite ajouteeeeeeeeee');
      //       })
      //     }
      //   }

      // for (let i=listeRDV.length;i<this.eventSettings.dataSource.length;i++){
          

      // }
      let j=0;
      let z=listeIndisponible.length;
      let lastElement=false;
      console.log(listeIndisponible);
      console.log(this.eventSettings.dataSource);
      if ((this.eventSettings.dataSource.length==listeRDV.length)&&(z!=0)){

        for(let m=0;m<z;m++){
          this.calendrierService.supprimerIndisponibilite(listeIndisponible[m]['id_indisponibilite']).subscribe(response=>{
            console.log('supprimé');
            this.eventdeleted=true;
            this.nbeventdeleted++;

          })
        }

      }
      else{
        for(let i=listeRDV.length;i<this.eventSettings.dataSource.length;i++)
         {
         
          if (j>=z){lastElement=true}
          while(j<z){
            if (this.eventSettings.dataSource[i]['Id']!=listeIndisponible[j]['id_indisponibilite']){
              console.log(this.eventSettings.dataSource[i]['Id']);
              console.log(listeIndisponible[j]['id_indisponibilite']);
              console.log('supression++');
              this.calendrierService.supprimerIndisponibilite(listeIndisponible[j]['id_indisponibilite']).subscribe(response=>{
                console.log('supprimé');
                this.eventdeleted=true;
                this.nbeventdeleted++;

              })

              j++;
              if (j>=z){lastElement=true}
             
            }
            else if ((this.eventSettings.dataSource[i]['Subject']!=listeIndisponible[j]['description'])||(this.eventSettings.dataSource[i]['StartTime']!=listeIndisponible[j]['dateDebutIndisponibilite'])||(this.eventSettings.dataSource[i]['EndTime']!=listeIndisponible[j]['dateFinIndisponibilite'])){
              console.log('modification');
              console.log(listeIndisponible[j]);
              this.calendrierService.getIndisponibiliteById(listeIndisponible[j]['id_indisponibilite']).subscribe(response=>{
                  response.description=this.eventSettings.dataSource[i].Subject;
                  response.dateDebutIndisponibilite=this.eventSettings.dataSource[i].StartTime;
                  response.dateFinIndisponibilite=this.eventSettings.dataSource[i].EndTime;
                  this.calendrierService.modifierIndisponibilite(response).subscribe(response=>{
                    console.log('modifié');
                    this.nbeventupdated++;
                    this.eventupdated=true;
                  })
              });
             
              j++;
              
              break;
              
            }
            else {
              console.log('ena houni');
              console.log(i);
              console.log(j);
              j++;
              break;
            }

          }
          if((j>=z)&&(lastElement==true)) {
            console.log('ajout');
            this.indisponibilite.description=this.eventSettings.dataSource[i].Subject;
            this.indisponibilite.dateDebutIndisponibilite=this.eventSettings.dataSource[i].StartTime;
            this.indisponibilite.dateFinIndisponibilite=this.eventSettings.dataSource[i].EndTime;
            this.indisponibilite.medecin=this.user.medecin;
            this.calendrierService.ajouterIndisponibilite(this.indisponibilite).subscribe(response=>{
              console.log('ajouté');
              this.eventadded=true;
              this.nbeventadded++;
            })
          }
        }
      } 
      this.userService.getUserByEmail(this.user.email).subscribe(response=>{
        sessionStorage.setItem('User',JSON.stringify(response));
        // this.user=response;
      })
    }
    
  }

  close(){
    this.userService.MessageBienvenue=false;
  }
  closeEvent(){
    this.eventupdated=false;
    this.eventdeleted=false;
    this.eventadded=false;
    this.nbeventadded=0;
    this.nbeventdeleted=0;
    this.nbeventupdated=0;
    this.userService.getUserByEmail(this.user.email).subscribe(response=>{
      sessionStorage.setItem('User',JSON.stringify(response));
      // this.user=response;
      this.chargerLesEvenements();
    })
    
  }
  async close2(){
    this.erreurManipulation=false;
    this.renitialisation=true;
    await delay(2000);
    this.ngOnInit();
  }
  testfunction(){
    console.log('teeeest shih');
  }
  

}

export let listeIndisponible: Object[] = [];
//   {
//       Id: 1,
//       Subject: 'indisponible',
      
//       StartTime: new Date(2019, 6, 7, 9, 30),
//       EndTime: new Date(2019, 6, 7, 11, 0),
//       CategoryColor: '#FF0000'
      
//   }, {
//       Id: 2,
//       Subject: 'Indisponible',
      
//       StartTime: new Date(2019, 7, 7, 12, 0),
//       EndTime: new Date(2019, 7, 7, 14, 0),
//       CategoryColor: 'red'
//   }];

  export let listeRDV: Object[] = [
    {
        Id: 1000,
        Subject: 'Consultation',
        
        StartTime: new Date(2019, 6, 5, 11, 30),
        EndTime: new Date(2019, 6, 5, 13, 0),
        CategoryColor: '#1aaa55'
    }, {
        Id: 1004,
        Subject: 'Consultation',
        // Location: 'Newyork City',
        StartTime: new Date(2019, 7, 5, 14, 0),
        EndTime: new Date(2019, 7, 5, 15, 0),
        CategoryColor: '#357cd2'
        
    }, {
      Id: 1005,
      Subject: 'Consultation',
      // Location: 'Newyork City',
      StartTime: new Date(2019, 7, 5, 15, 0),
      EndTime: new Date(2019, 7, 5, 16, 0),
      CategoryColor: '#357cd2'
      
  }]

    export let scheduleData3: Object[]=[]

    function delay(ms: number) {
      return new Promise( resolve => setTimeout(resolve, ms) );
  }

  export class Indisponibilite{
    description:String;
    dateDebutIndisponibilite:String;
    dateFinIndisponibilite:String;
    medecin:Medecin;
    id_disponibilite:number;
    constructor(){}
  }
  
  