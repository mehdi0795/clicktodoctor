import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { error } from 'util';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {

  board : string;
  errorMessage : string;

  constructor(private userService : UserService) { }

  ngOnInit() {

    this.userService.getEspacePatient().subscribe(
      data => {
        console.log("Accès Patient");
        this.board = data;
      },
      error => {
        this.errorMessage = `${error.status} : ${JSON.parse(error.error).message}`;
      }
      
    );
  }

}
