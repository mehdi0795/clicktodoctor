import { BrowserModule } from '@angular/platform-browser';
import { FormsModule} from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';
import { PatientComponent } from './patient/patient.component';
import { MedecinComponent } from './medecin/medecin.component';
import { HttpInterceptorProviders } from './auth/auth-interceptor';
import { TestComponent } from './test/test.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule, MatSelectModule, MatInputModule, MatNativeDateModule} from '@angular/material';
import { MatIconModule } from "@angular/material/icon";
import { ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import { EspaceMedecinComponent } from './espace-medecin/espace-medecin.component';
import {MatTableModule} from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material';
import {MatMenuModule} from '@angular/material/menu';
import {MatTreeModule} from '@angular/material/tree';
import {MatSortModule} from '@angular/material/sort';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModifierCompteComponent } from './modifier-compte/modifier-compte.component';
import {MatRadioModule} from '@angular/material/radio';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatBadgeModule} from '@angular/material/badge';
import { CalendrierMedecinComponent } from './calendrier-medecin/calendrier-medecin.component';

import { CommonModule } from '@angular/common';

// import { FlatpickrModule } from 'angularx-flatpickr';
// import { CalendarModule, DateAdapter } from 'angular-calendar';
// import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { DetailsPatientComponent } from './details-patient/details-patient.component';
import { AppelVideoComponent } from './appel-video/appel-video.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {FullCalendarModule} from '@fullcalendar/angular';
import { Calendrier2Component } from './calendrier2/calendrier2.component';

 //calendrier2
 import { TreeViewModule } from '@syncfusion/ej2-angular-navigations';

 import { DropDownListAllModule, MultiSelectAllModule } from '@syncfusion/ej2-angular-dropdowns';
 
 import { MaskedTextBoxModule, UploaderAllModule } from '@syncfusion/ej2-angular-inputs';
 
 import { ToolbarAllModule, ContextMenuAllModule } from '@syncfusion/ej2-angular-navigations';
 
 import { ButtonAllModule  } from '@syncfusion/ej2-angular-buttons';
 
 import { CheckBoxAllModule } from '@syncfusion/ej2-angular-buttons';
 
 import { DatePickerAllModule, TimePickerAllModule, DateTimePickerAllModule } from '@syncfusion/ej2-angular-calendars';
 
 import { NumericTextBoxAllModule } from '@syncfusion/ej2-angular-inputs';
 
 import { ScheduleAllModule, RecurrenceEditorAllModule } from '@syncfusion/ej2-angular-schedule';
 import {MatCardModule} from '@angular/material/card';
 //import { HttpModule } from '@angular/http';
 

 
 import { RouterModule } from '@angular/router';
 

 

 

 //finCalendrier2



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    AdminComponent,
    PatientComponent,
    MedecinComponent,
    TestComponent,
    EspaceMedecinComponent,
    ModifierCompteComponent,
    CalendrierMedecinComponent,
    DetailsPatientComponent,
    AppelVideoComponent,
    Calendrier2Component
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatMenuModule,
    MatTreeModule,
    MatSortModule,
    NgbModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatBadgeModule,
    CommonModule,
    FormsModule,
    NgbModalModule,
    // FlatpickrModule.forRoot(),
    // CalendarModule.forRoot({
    //   provide: DateAdapter,
    //   useFactory: adapterFactory
    // })
    MatProgressBarModule,
    FullCalendarModule,
    //calendrier2
    TreeViewModule,
    DropDownListAllModule, MultiSelectAllModule,
    MaskedTextBoxModule, UploaderAllModule,
    ToolbarAllModule, ContextMenuAllModule,
    ButtonAllModule,CheckBoxAllModule,
    DatePickerAllModule, TimePickerAllModule, DateTimePickerAllModule,
    NumericTextBoxAllModule,
    ScheduleAllModule, RecurrenceEditorAllModule,
    MatCardModule


    
    
    
   
  ],

  providers: [HttpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
