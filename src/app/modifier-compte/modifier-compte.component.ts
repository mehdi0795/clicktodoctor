import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { NouveauMedecin, User, Specialite, Medecin } from '../test/test.component';
import { MedecinService } from '../services/medecin.service';
import { PremiereInscription } from '../medecin/medecin.component';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modifier-compte',
  templateUrl: './modifier-compte.component.html',
  styleUrls: ['./modifier-compte.component.css']
})
export class ModifierCompteComponent implements OnInit {



  //nouveau
  user=new User();
  messageErreur=false;
  modifierInscription=new PremiereInscription();
  specialites = new FormControl();

  // SpecialiteList: string[] = ['Dermatologie', 'Hématologie', 'Neurologie', 'Urologie', 'Rhumatologie', 'Ophtalmologie'];
  SpecialiteList: Specialite[] ;

  specsMedecin:string[]=[];
  favoriteSeason: string;
  sexes: string[] = ['Homme', 'Femme'];
  firstPart=true;
  secondPart=false;
  hide = true;
  // medecinUpdated=new MedecinModifie();
  medecin=new Medecin();
  regions: string[] = [
    'Grand_Est',
	'Nouvelle_Aquitaine',
	'Auvergne_Rhône_Alpes',
	'Bourgogne_Franche_Comté',
	'Bretagne',
	'Centre_Val_de_Loire',
	'Corse',
	'Île_de_France',
	'Occitanie',
	'Hauts_de_France',
	'Normandie',
	'Pays_de_la_Loire',
	'Provence_Alpes_Côte_d_Azur'
  ];

  departementsParRegion: string[];

  

  departements:departement[]=[
    {region:'Grand_Est',departement:['Ardennes',	
      'Aube',
      'Marne',	
      'Haute_Marne',
      'Meurthe_et_Moselle',
      'Meuse',
      'Moselle',
      'Bas_Rhin',
      'Haut_Rhin',
      'Vosges']},
    {region:'Nouvelle_Aquitaine',departement:['Charente',
      'Charente_Maritime',
      'Corrèze',
      'Creuse',
      'Deux_Sèvres',
      'Dordogne',
      'Gironde',
      'Landes',
      'Lot_et_Garonne',
      'Pyrénées_Atlantiques',
      'Haute_Vienne',
      'Vienne']},
    {region:'Auvergne_Rhône_Alpes',departement:['Ain',
      'Allier',
      'Ardèche',
      'Cantal',
      'Drôme',
      'Haute_Loire',
      'Isère',
      'Loire',
      'Puy_de_Dôme',
      'Rhône',
      'Savois',
      'Haute_Savoie']},
    {region:'Bourgogne_Franche_Comté',departement:['']},
    {region:'Bretagne',departement:['']},
    {region:'Centre_Val_de_Loire',departement:['']},
    {region:'Corse',departement:['']},
    {region:'Île_de_France',departement:['']},
    {region:'Occitanie',departement:['']},
    {region:'Hauts_de_France',departement:['']},
    {region:'Normandie',departement:['']},
    {region:'Pays_de_la_Loire',departement:['']},
    {region:'Provence_Alpes_Côte_d_Azur',departement:['']}

  ]

  codesPostalParDepartement:string[];

  codesPostal:codePostal[]=[
    {departement:'Ardennes',codepostal:['_08090(Ardennes)',
      '_08190(Ardennes)',
      '_08310(Ardennes)']},
    {departement:'Aube',codepostal:['_10160(Aube)',
      '_10700(Aube)',
      '_10140(Aube)']},
    {departement:'Marne',codepostal:['_51150(Marne)',
      '_51260(Marne)',
      '_51120(Marne)']}
    

  ]
  constructor(private medecinService:MedecinService,private userService:UserService,private router:Router) { }

  ngOnInit() {
    this.user=JSON.parse(sessionStorage.getItem('User'));
    this.medecin=this.user.medecin
    console.log(this.user);
    console.log('les specs');
    for(let i=0;i<this.user.medecin.specialites.length;i++){
      console.log(this.user.medecin.specialites[i].nomSpecialite);
      this.specsMedecin[i]=this.user.medecin.specialites[i].nomSpecialite;
    }
    
    console.log(this.user)
    
    console.log('specsmedecin');
    console.log(this.specsMedecin);

    this.medecinService.getAllSpecialites().subscribe(response=>{
      this.SpecialiteList=response;
      // console.log(this.SpecialiteList);
    });

  }

  previousPart(){
    this.secondPart=false;
    this.firstPart=true;

  }
  close(){
    this.messageErreur=false;
  }
  nextPart(){
    this.firstPart=false;
    this.secondPart=true;
    console.log(this.specsMedecin);
    
  }

  afficherDepartements(region){
    for(let i=0;i<this.departements.length;i++)
    {
        if (this.departements[i].region === region)
        {
          console.log('ok');
          console.log(i);
          this.departementsParRegion=this.departements[i].departement;
        }
    }
  }
afficherCodepostal(dep){

  for(let i=0;i<this.codesPostal.length;i++)
  {
      if (this.codesPostal[i].departement === dep)
      {
        
        this.codesPostalParDepartement=this.codesPostal[i].codepostal;
      }
  }
  
}

validerModification(){
  // this.medecinUpdated.nouveauMedecin=this.medecin;
   console.log(this.user);
   console.log('matricule');
   console.log(this.user.matricule);
   this.modifierInscription.medecin=this.user.medecin;
   this.modifierInscription.specs=this.specsMedecin;
   console.log('specs avant modification');
   console.log(this.modifierInscription.specs);
   this.modifierInscription.user=this.user;
   console.log(this.modifierInscription);
   this.userService.modifierMedecin(this.user.matricule,this.modifierInscription).subscribe(response=>{
    //  console.log('bravooooooooooo MAJ EFFECTUEE');
    this.userService.miseAJour=true;
    this.userService.connexion(this.modifierInscription.user.email).subscribe(response=>{sessionStorage.setItem('User',JSON.stringify(response));})
    this.router.navigate(["/espaceMedecin"]);
    

   },
   erreur=>{
     this.messageErreur=true;
   })
  

}

}

export class FormFieldErrorExample {
  email = new FormControl('', [Validators.required, Validators.email]);

  getErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' :
            '';
  }
}

export interface departement{ 

  region:string;
  departement:string[];
}

export interface codePostal{
  departement:string;
  codepostal:string[];
}

// export class MedecinModifie {

//   nouveauMedecin:NouveauMedecin;
//   sexe:string;
//   dateDeNaissance:string;
//   adresse:string;
//   region:string;
//   departement:string;
//   codePostal:string;
//   dateDiplome:string;
//   universite:string;

//   constructor(){}

// }