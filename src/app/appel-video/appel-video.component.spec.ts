import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppelVideoComponent } from './appel-video.component';

describe('AppelVideoComponent', () => {
  let component: AppelVideoComponent;
  let fixture: ComponentFixture<AppelVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppelVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppelVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
