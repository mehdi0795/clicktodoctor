import { Component, OnInit } from '@angular/core';
import { LoginInfo } from '../auth/LoginInfo';
import { AuthService } from '../auth/auth.service';
import { TokenStorageService } from '../auth/token-storage.service';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { TestComponent } from '../test/test.component';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form : any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];

  EmailNotFound=false;
  wrongPassword=false;
  private loginInfo : LoginInfo;

  
  

  constructor(private _router : Router ,
    private authService: AuthService,
     private tokenStorage: TokenStorageService,
     private userService: UserService
     
     ) { }

  ngOnInit() {
    if(this.tokenStorage.getToken()){
      
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getAuthorities();
      console.log("isLoggedIn",this.isLoggedIn)

    }
  }

  onSubmit(){
    console.log(this.form);

    this.loginInfo = new LoginInfo(this.form.username, this.form.password);

    this.authService.login(this.loginInfo).subscribe(
      data => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUsername(data.username);
        this.tokenStorage.saveAuthorities(data.authorities);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getAuthorities();
        this.reloadPage();
        
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
        this.isLoginFailed = true;
      }
      
    );
  }

  reloadPage(){
    window.location.reload();
    this._router.navigate(['home']);
  }

  close(){
    this.EmailNotFound=false;
    this.wrongPassword=false;
  }

  Connexion(){
    // console.log(this.form.password);
    // console.log(this.form.email);
    // sessionStorage.setItem('isMedecin',JSON.stringify(true));
    // //  this.test.EnregistrerMedecin();
    // // this.appComponent.isMedecinConnected();
    //  this._router.navigate(['/espaceMedecin'])
      console.log(this.form.email);
    this.userService.connexion(this.form.email).subscribe(response=>{
      console.log(response);
      
      if (response != null)
      {
        // console.log(response.password);
        // console.log(response.medecin);
        // console.log('helllo');
        if (response.password==this.form.password)
        {
            console.log('its okay');
            sessionStorage.setItem('User',JSON.stringify(response));
            
            this.userService.connexionMedecin();


        }
        else{
            this.wrongPassword=true;  
        }


      }
      else { 

        this.EmailNotFound=true;

      }
      
    })

  }

}
